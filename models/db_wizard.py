### we prepend t_ to tablenames and f_ to fieldnames for disambiguity


########################################
db.define_table('t_user_results',
    Field('f_user_id', type='reference auth_user',
          label=T('User Id')),
    Field('f_type', type='string',
          label=T('Type')),
    Field('f_screen_width', type='float',
          label=T('Screen Width')),
    Field('f_screen_height', type='float',
          label=T('Screen Height')),
    Field('f_feedback', type='boolean',
          label=T('Feedback')),
    auth.signature,
    format='%(id)s',
    migrate=settings.migrate)

db.define_table('t_user_results_archive',db.t_user_results,Field('current_record','reference t_user_results',readable=False,writable=False))

########################################
db.define_table('t_round',
    Field('f_test', type='reference t_user_results',
          label=T('Test Id')),
    Field('f_part', type='string',
          label=T('Part')),
    Field('f_attempt', type='integer',
          label=T('Attempt')),
    Field('f_expected_value', type='float',
          label=T('Expected Value')),
    Field('f_user_value', type='float',
          label=T('User Value')),
    Field('f_score', type='float',
          label=T('Score')),
    Field('f_x_pos', type='float',
          label=T('X Position of Active Element')),
    Field('f_y_pos', type='float',
          label=T('Y Position of Active Element')),
    auth.signature,
    format='%(id)s',
    migrate=settings.migrate)

db.define_table('t_round_archive',db.t_round,Field('current_record','reference t_round',readable=False,writable=False))

########################################
db.define_table('t_other_element',
    Field('f_round', type='reference t_round',
          label=T('Round Id')),
    Field('f_x_pos', type='float',
          label=T('X Position of Element')),
    Field('f_y_pos', type='float',
          label=T('Y Position of Element')),
    Field('f_value', type='float',
          label=T('Value')),
    auth.signature,
    format='%(f_user_id)s',
    migrate=settings.migrate)

db.define_table('t_other_element_archive',db.t_other_element,Field('current_record','reference t_other_element',readable=False,writable=False))
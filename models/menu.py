response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
response.menu = [
(T('Index'),URL('default','index')==URL(),URL('default','index'),[]),
(T('User Results'),URL('default','user_results_manage')==URL(),URL('default','user_results_manage'),[]),
(T('User Info'),URL('default','user_info_manage')==URL(),URL('default','user_info_manage'),[]),
]
from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'FCAI Psychometrics App'
settings.subtitle = 'powered by web2py'
settings.author = 'Dan Burger, Gordon Kiesling, Kurt Jones'
settings.author_email = 'dan.burger@vanderbilt.edu'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = '53bd7901-15df-4a50-808d-107e9afeefe1'
settings.email_server = 'localhost'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []

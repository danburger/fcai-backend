# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()
    
def results():
    user_ID = request.get_vars['user_ID']
    key = request.get_vars['key']
    if valid_key(key, user_ID):
        user_ID = int(user_ID)
    elif auth.user_id:
        user_ID = int(auth.user_id)
    else:
        redirect("/"+request.application+"/default/user/login?_next="+request.url)
    user_results = db(db.t_user_results.f_user_id==user_ID).select(orderby=~db.t_user_results.id)
    rounds = {}
    other_elements = {}
    for result in user_results:
        rounds[result.id] = db(db.t_round.f_test==result.id).select()
        for r in rounds[result.id]:
            other_elements[r.id] = db(db.t_other_element.f_round==r.id).select()
    return dict(user_results=user_results,rounds=rounds,other_elements=other_elements)

def error():
    return dict()




# Now for my stuff
def register():
    user_email = request.get_vars['email']
    user_pass = request.get_vars['password']
    if user_email != None and user_pass != None:
        key=session_key_gen()
        ret = auth.register_bare(first_name=None,last_name=None,email=user_email, password=user_pass, session_key=key)
        if ret == False:
            return 'Email Already Taken'
        else:
            return 'Registration Successful, key=' + key + ' user_ID=' + str(ret.id)
    else:
        return 'bad parameters received'


def login():
    user_email = request.get_vars['email']
    user_pass = (request.get_vars['password'])
    if user_email != None and user_pass != None:
        ret = auth.login_bare(user_email,user_pass)
        if ret:
            key = session_key_gen()
            db(db.auth_user.email == user_email).update(session_key=key)
            return "Login Successful, key=" + key + ' user_ID=' + str(ret.id)
        else:
            return "Invalid email or password"
    else:
        return "no variables passed"

def bio_call():
    user_ID = request.get_vars['user_ID']
    key = request.get_vars['key']
    if valid_key(key, user_ID):
        b_age = int(request.get_vars['age'])
        b_gender = str(request.get_vars['gender'])
        b_ethnicity = str(request.get_vars['ethnicity'])
        b_education = str(request.get_vars['education'])
        k = db(db.auth_user.id == user_ID).update(age=b_age, gender=b_gender, ethnicity=b_ethnicity, education=b_education)
        return 'demographic info successfully stored in database'
    else:
        return 'invalid key passed'

def initiate_test():
    key = request.get_vars['key']
    user_ID = request.get_vars['user_ID']
    f_type = str(request.get_vars['type'])
    f_screen_width = float(request.get_vars['screen_width'])
    f_screen_height = float(request.get_vars['screen_height'])
    f_feedback = str(request.get_vars['type']).lower() == "true"
    if valid_key(key, user_ID):
        test_ID = db.t_user_results.insert(f_user_id=int(user_ID),f_type=f_type,f_screen_width=f_screen_width,
            f_screen_height=f_screen_height,f_feedback=f_feedback)
        return 'test initiated, test ID = ' + str(test_ID)
    else:
        return 'invalid key passed'

def log_scores():
    key = request.get_vars['key']
    user_ID = request.get_vars['user_ID']
    if valid_key(key, user_ID):
        # pull variables from url
        score = float(request.get_vars['score'])
        test_name = request.get_vars['test_name']
        test_ID = int(request.get_vars['test_ID'])
        attempt = int(request.get_vars['attempt'])
        expected_value = float(request.get_vars['expected_value'])
        user_value = float(request.get_vars['user_value'])
        x_pos = float(request.get_vars['x_pos'])
        y_pos = float(request.get_vars['y_pos'])
        other_xpos = request.get_vars['other_xpos']
        other_ypos = request.get_vars['other_ypos']
        other_value = request.get_vars['other_xpos']
        if type(other_xpos) == list:
            other_elements = [dict(x=float(x),y=float(y),val=float(val)) for x,y,val in zip(other_xpos,other_ypos,other_value)]
        elif type(other_xpos) == str:
            other_elements = [dict(x=float(other_xpos),y=float(other_ypos),val=float(other_value))]
        else:
            other_elements = []
        # submit to database
        round_ID = db.t_round.insert(f_test=test_ID,f_part=test_name,f_attempt=attempt,f_expected_value=expected_value,
            f_user_value=user_value,f_x_pos=x_pos,f_y_pos=y_pos,f_score=score)
        for item in other_elements:
            db.t_other_element.insert(f_round=round_ID,f_x_pos=item['x'],f_y_pos=item['y'],f_value=item['val'])
        # receipt for app console
        return 'score logged successfully'
    else:
        return 'invalid key passed'

def questionnaire():
    key = request.get_vars['key']
    user_ID = request.get_vars['user_ID']
    if valid_key(key, user_ID):
        difficulty = request.get_vars['difficulty']
        engagement = request.get_vars['engagement']
        comments = request.get_vars['comments']
        db(db.auth_user.id == user_ID).update(difficulty=difficulty, engagement=engagement, comments=comments)
        return 'questionnaire answers logged successfully'
    else:
        return 'invalid key passed'

def logout():
    key = request.get_vars['key']
    user_ID = request.get_vars['user_ID']
    if valid_key(key, user_ID):
        new_key = session_key_gen()
        db(db.auth_user.session_key == key).update(session_key=new_key)
        return 'logout successful'
    else:
        return 'invalid key passed'

import random
import string
def session_key_gen():
    session_key = ''
    for x in range(16):
        if (random.random() > .5):
            session_key += random.choice(string.letters)
        else:
            session_key += random.choice(string.digits)
    return session_key

def valid_key(key, user_ID):
    found_key = ''
    rows = db(db.auth_user).select()
    for row in rows.find(lambda row: row.session_key == key and row.id==int(user_ID)):
        found_key += row.session_key
    return found_key == key

def privacy():
    return dict()
